################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/CANOpen/CANopen.c \
../source/CANOpen/CO_Emergency.c \
../source/CANOpen/CO_HBconsumer.c \
../source/CANOpen/CO_NMT_Heartbeat.c \
../source/CANOpen/CO_OD.c \
../source/CANOpen/CO_PDO.c \
../source/CANOpen/CO_SDO.c \
../source/CANOpen/CO_SDOmaster.c \
../source/CANOpen/CO_SYNC.c \
../source/CANOpen/CO_driver.c \
../source/CANOpen/CO_trace.c \
../source/CANOpen/application.c \
../source/CANOpen/crc16-ccitt.c \
../source/CANOpen/eeprom.c \
../source/CANOpen/main.c 

C_DEPS += \
./source/CANOpen/CANopen.d \
./source/CANOpen/CO_Emergency.d \
./source/CANOpen/CO_HBconsumer.d \
./source/CANOpen/CO_NMT_Heartbeat.d \
./source/CANOpen/CO_OD.d \
./source/CANOpen/CO_PDO.d \
./source/CANOpen/CO_SDO.d \
./source/CANOpen/CO_SDOmaster.d \
./source/CANOpen/CO_SYNC.d \
./source/CANOpen/CO_driver.d \
./source/CANOpen/CO_trace.d \
./source/CANOpen/application.d \
./source/CANOpen/crc16-ccitt.d \
./source/CANOpen/eeprom.d \
./source/CANOpen/main.d 

OBJS += \
./source/CANOpen/CANopen.obj \
./source/CANOpen/CO_Emergency.obj \
./source/CANOpen/CO_HBconsumer.obj \
./source/CANOpen/CO_NMT_Heartbeat.obj \
./source/CANOpen/CO_OD.obj \
./source/CANOpen/CO_PDO.obj \
./source/CANOpen/CO_SDO.obj \
./source/CANOpen/CO_SDOmaster.obj \
./source/CANOpen/CO_SYNC.obj \
./source/CANOpen/CO_driver.obj \
./source/CANOpen/CO_trace.obj \
./source/CANOpen/application.obj \
./source/CANOpen/crc16-ccitt.obj \
./source/CANOpen/eeprom.obj \
./source/CANOpen/main.obj 

OBJS__QUOTED += \
"source\CANOpen\CANopen.obj" \
"source\CANOpen\CO_Emergency.obj" \
"source\CANOpen\CO_HBconsumer.obj" \
"source\CANOpen\CO_NMT_Heartbeat.obj" \
"source\CANOpen\CO_OD.obj" \
"source\CANOpen\CO_PDO.obj" \
"source\CANOpen\CO_SDO.obj" \
"source\CANOpen\CO_SDOmaster.obj" \
"source\CANOpen\CO_SYNC.obj" \
"source\CANOpen\CO_driver.obj" \
"source\CANOpen\CO_trace.obj" \
"source\CANOpen\application.obj" \
"source\CANOpen\crc16-ccitt.obj" \
"source\CANOpen\eeprom.obj" \
"source\CANOpen\main.obj" 

C_DEPS__QUOTED += \
"source\CANOpen\CANopen.d" \
"source\CANOpen\CO_Emergency.d" \
"source\CANOpen\CO_HBconsumer.d" \
"source\CANOpen\CO_NMT_Heartbeat.d" \
"source\CANOpen\CO_OD.d" \
"source\CANOpen\CO_PDO.d" \
"source\CANOpen\CO_SDO.d" \
"source\CANOpen\CO_SDOmaster.d" \
"source\CANOpen\CO_SYNC.d" \
"source\CANOpen\CO_driver.d" \
"source\CANOpen\CO_trace.d" \
"source\CANOpen\application.d" \
"source\CANOpen\crc16-ccitt.d" \
"source\CANOpen\eeprom.d" \
"source\CANOpen\main.d" 

C_SRCS__QUOTED += \
"../source/CANOpen/CANopen.c" \
"../source/CANOpen/CO_Emergency.c" \
"../source/CANOpen/CO_HBconsumer.c" \
"../source/CANOpen/CO_NMT_Heartbeat.c" \
"../source/CANOpen/CO_OD.c" \
"../source/CANOpen/CO_PDO.c" \
"../source/CANOpen/CO_SDO.c" \
"../source/CANOpen/CO_SDOmaster.c" \
"../source/CANOpen/CO_SYNC.c" \
"../source/CANOpen/CO_driver.c" \
"../source/CANOpen/CO_trace.c" \
"../source/CANOpen/application.c" \
"../source/CANOpen/crc16-ccitt.c" \
"../source/CANOpen/eeprom.c" \
"../source/CANOpen/main.c" 


