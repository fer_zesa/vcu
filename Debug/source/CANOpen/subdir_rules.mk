################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
source/CANOpen/CANopen.obj: ../source/CANOpen/CANopen.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CANopen.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_Emergency.obj: ../source/CANOpen/CO_Emergency.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_Emergency.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_HBconsumer.obj: ../source/CANOpen/CO_HBconsumer.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_HBconsumer.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_NMT_Heartbeat.obj: ../source/CANOpen/CO_NMT_Heartbeat.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_NMT_Heartbeat.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_OD.obj: ../source/CANOpen/CO_OD.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_OD.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_PDO.obj: ../source/CANOpen/CO_PDO.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_PDO.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_SDO.obj: ../source/CANOpen/CO_SDO.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_SDO.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_SDOmaster.obj: ../source/CANOpen/CO_SDOmaster.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_SDOmaster.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_SYNC.obj: ../source/CANOpen/CO_SYNC.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_SYNC.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_driver.obj: ../source/CANOpen/CO_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_driver.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/CO_trace.obj: ../source/CANOpen/CO_trace.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/CO_trace.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/application.obj: ../source/CANOpen/application.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/application.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/crc16-ccitt.obj: ../source/CANOpen/crc16-ccitt.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/crc16-ccitt.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/eeprom.obj: ../source/CANOpen/eeprom.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/eeprom.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/CANOpen/main.obj: ../source/CANOpen/main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000/include/CANOpen" --include_path="C:/Users/Tomislav/workspace_v7/VCU_1000" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/CANOpen/main.d_raw" --obj_directory="source/CANOpen" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


